set -U fish_greeting

if [ (hostname) = "gogo" ]
	set -gx LC_ALL es_ES.utf8
	set -gx LANG es_ES.utf8
else
	set -gx LC_ALL en_US.UTF-8
	set -gx LANG en_US.UTF-8
end

set -gx TERMINFO_DIRS /usr/share/terminfo:/usr/local/share/terminfo
set -gx CDPATH . ~/pers ~/proj ~/work

if [ (uname -s) = "OpenBSD" ]
	set -gx KITTY_SHELL_INTEGRATION "no-sudo"
end

if [ (uname -s) = "FreeBSD" ]
	set -gx TERMINFO "$TERMINFO_DIRS"
else
	set -gx TERMPATH "$TERMINFO_DIRS"
end

# OpenBSD
fish_add_path /usr/games

# Adevinta
fish_add_path $HOME/.adv/bin

# Homebrew
fish_add_path /opt/homebrew/bin

# Golang
fish_add_path $HOME/go/bin

# Python
fish_add_path $HOME/.local/bin

# Krew
if set -q KREW_ROOT
	fish_add_path $KREW_ROOT/bin
else
	fish_add_path $HOME/.krew/bin
end

# Gettext
set -l gettext /opt/homebrew/Cellar/gettext/*/bin/
fish_add_path $gettext
fish_add_path $HOME/bin

# Ruby
fish_add_path /opt/homebrew/lib/ruby/gems/2.7.0/bin
fish_add_path /opt/homebrew/opt/ruby/bin

# Cask
fish_add_path /Users/william.orr/.cask/bin

# Curl
fish_add_path /opt/homebrew/opt/curl/bin

# LLVM
fish_add_path /opt/homebrew/opt/llvm/bin

# Rust
fish_add_path $HOME/.cargo/bin

# Poetry
fish_add_path $HOME/.poetry/bin

# Gnupg on macOS
fish_add_path /opt/homebrew/opt/gnupg@2.2/bin

if [ -d "$HOME/go" ]
	set -gx GOPATH "$HOME/go"
end

if [ -d "$HOME/rust" ]
	set -gx RUST_SRC_PATH "$HOME/rust/library"
end

if test -x /opt/homebrew/bin/mise -o -x /usr/bin/mise
	mise activate fish | source
	mise hook-env -s fish | source
end

function adevinta_setup
	set -gx GITHUB_HOST github.mpi-internal.com
	set -gx GITHUB_TOKEN (security find-internet-password -s $GITHUB_HOST -w)
	set -gx GOPRIVATE "github.mpi-internal.com/*"
	set -gx ARTIFACTORY_HOST artifactory.mpi-internal.com
	set -gx ARTIFACTORY_CONTEXT "https://$ARTIFACTORY_HOST/artifactory"
	set -gx ARTIFACTORY_USER "william.orr@adevinta.com"
	set -gx ARTIFACTORY_PWD (security find-internet-password -s $ARTIFACTORY_HOST -w)
	set -gx ARTIFACTORY_NPM_SECRET "$(echo -n $ARTIFACTORY_USER:$ARTIFACTORY_PWD | base64)"
end

if status is-interactive
	if test -d ~/.gnupg
		set -gx GPG_TTY (tty)
		set -l pinentry_program /usr/local/bin/pinentry-gtk-2
		if test -x /usr/bin/pinentry-gnome-3
			set pinentry_program /usr/bin/pinentry-gnome3
		end
		if test -x /opt/homebrew/bin/pinentry-mac
			set pinentry_program /opt/homebrew/bin/pinentry-mac
		end

		eval (gpg-agent --daemon --pinentry-program "$pinentry_program" | sed -e "s/\(.*\)=\(.*\); export/set -x \1 \2/")
	end

	set -gx GTAGSLABEL pygments
	set -gx BAT_STYLE numbers
	set -gx BAT_THEME "Monokai Extended Light"
	set -gx PAGER less
	set -gx DOCKER_SCAN_SUGGEST false

	if [ -d $HOME/.adv/ ]
		adevinta_setup
	end

	if [ $SSH_CONNECTION ]
		set -gx EDITOR vim
	else
		set -gx EDITOR 'emacsclient'
	end

	abbr -a pg pijul

	if command -q delta
		alias diff delta
	end

	# Vi mode and vim cursor emulation

	# Emulates vim's cursor shape behavior
	# Set the normal and visual mode cursors to a block
	set fish_cursor_default block

	# Set the insert mode cursor to a line
	set fish_cursor_insert line

	# Set the replace mode cursor to an underscore
	set fish_cursor_replace_one underscore

	# The following variable can be used to configure cursor shape in
	# visual mode, but due to fish_cursor_default, is redundant here
	set fish_cursor_visual block

	# Don't show the vi mode in the prompt
	functions --erase fish_mode_prompt

	# this is done so that I can get kitty integ in remote hosts
	#alias ssh 'kitty +kitten ssh'

	# icat is way too handy to not have in an alias
	alias icat 'kitty +kitten icat'
	complete -c icat --wraps 'kitty +kitten icat'

	# hyperlinked grep!!
	function rg --wraps rg
		kitty +kitten hyperlinked_grep $argv
	end

	# let's try some abbreviations
	# kubectl pods
	abbr -a k 'kubectl'
	abbr -a kg 'kubectl get'
	abbr -a kgp 'kubectl get pod'
	abbr -a kga 'kubectl get --all-namespaces'
	abbr -a kgall 'kubectl get --all-namespaces all'
	abbr -a kd 'kubectl describe'
	abbr -a kdp 'kubectl describe pod'
	# kubectl apply
	abbr -a kap 'kubectl apply'
	# kubectl delete
	abbr -a krm 'kubectl delete'
	abbr -a krmf 'kubectl delete -f'
	# kubectl services
	abbr -a kgs 'kubectl get service'
	abbr -a kds 'kubectl describe service'
	# kubectl deployments
	abbr -a kgd 'kubectl get deployments'
	abbr -a kdd 'kubectl describe deployments'
	# kubectl jobs
	abbr -a kgj 'kubectl get jobs'
	abbr -a kdj 'kubectl describe jobs'
	# kubectl nodes
	abbr -a kgn 'kubectl get nodes'
	abbr -a kdn 'kubectl describe node'
	# kubectl misc
	abbr -a kl 'kubectl logs'
	abbr -a kei 'kubectl exec -it'
	abbr -a kc 'kubectl ctx'
	abbr -a kn 'kubectl ns'

	abbr -a dock 'docker buildx build --push -t containers.mpi-internal.com/ephemeral/platformservicescre/(basename $PWD):latest --platform linux/arm64,linux/amd64 .'
end

function fish_prompt
	set -l st $status  # must be first line

	if [ $TERM = "dumb" ]
		echo "\$"
		return
	end

	set -l prompt_symbol '$'
	set -l prefix ''
	set -l hostn ''
	fish_is_root_user; and set prompt_symbol '#'

	# Small prompt optimization to ensure that we don't
	# spawn unnecessary shells
	set -l prompt_items (prompt_pwd; date +%H:%M:%S)

	if [ $SCRIPT ]
		set prefix "(log: $SCRIPT) "
	end

	set -l color purple
	if [ $SSH_CONNECTION ]
		set color green
		set hostn (hostname)"|"
	end

	echo (set_color $color)$prefix$hostn$prompt_items[1]\|$prompt_items[2]\|$st$prompt_symbol (set_color normal)
end

function fish_user_key_bindings
	# C-f for completing suggestions
	bind -M insert \cF accept-autosuggestion
	# M-f for completing one word of suggestions
	bind -M insert \u0192 forward-bigword

	# preserves vi keybindings if there are conflicts
	fish_vi_key_bindings --no-erase insert
end
