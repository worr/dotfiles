function kubectl
		set -l real_kubectl (which kubectl)
		set -l o 1
		set -l found 1

		if test (count $argv) -lt 4
				$real_kubectl $argv
				return
		end

		if test $argv[1] != get
				$real_kubectl $argv
				return
		end

		for arg in $argv
				if test $arg = "-o" || test $arg = "--output"
						set o 0
						continue
				end

				if test $o -eq 0
						if test $arg = yaml
								set found 0
								break
						end
				end

				if test $arg = "--output=yaml"
						set found 0
						break
				end

				set o 1
		end

		if test $found -eq 0
				$real_kubectl $argv | bat -l yaml
				return
		else
				$real_kubectl $argv
				return
		end
end
