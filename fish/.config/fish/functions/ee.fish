function ee
    if pgrep emacs >/dev/null
        emacsclient -n $argv
    else
        set -l osname (uname -s)
        if test $osname = "Darwin"
            open -a Emacs

            set -l ret -1
            while test $ret -ne 0
                emacsclient -n $argv 2>/dev/null
                set ret $status
                sleep 1
            end
        end
    end
end
