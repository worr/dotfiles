FILES := \
	$(HOME)/.spectrwm.conf \
	$(HOME)/.vimrc \
	$(HOME)/.cvsrc \
	$(HOME)/.profile \
	$(HOME)/.kshrc \
	$(HOME)/.config/kitty \
	$(HOME)/.config/git \
	$(HOME)/.config/fish \
	$(HOME)/.bashrc \
	$(HOME)/.bash_profile \
	$(HOME)/.mbsyncrc \
	$(HOME)/.mbsync-imap-password.gpg \
	$(HOME)/.spacemacs \
	$(HOME)/.config/yamllint

.PHONY: all

all: $(FILES)

$(HOME)/.kshrc $(HOME)/.profile: ksh
	stow --dotfiles ksh

$(HOME)/.vimrc: vim
	stow --dotfiles vim

$(HOME)/.spectrwm.conf: spectrwm
	stow --dotfiles spectrwm

$(HOME)/.cvsrc: cvs
	stow --dotfiles cvs

$(HOME)/.config/kitty: kitty
	stow kitty

$(HOME)/.config/git: git
	stow git

$(HOME)/.config/fish: fish
	stow fish

$(HOME)/.bashrc $(HOME)/.bash_profile: bash
	stow --dotfiles bash

$(HOME)/.mbsyncrc $(HOME)/.mbsync-imap-password.gpg: mbsync
	stow --dotfiles mbsync

$(HOME)/.spacemacs: spacemacs
	stow --dotfiles spacemacs

$(HOME)/.config/yamllint: yamllint
	stow yamllint
