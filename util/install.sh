#!/usr/bin/env bash

GIT=`which git`
MAKE=`which gmake`
SUDO=`which sudo`
INSTALL=`which install`
GREP=`which grep`
CRONTAB=`which crontab`

CRON_SCRIPT="${HOME}/.files/util/cron.sh"
CRON_TEMP="${HOME}/.files/util/.crontmp"
INSTALL_DIR="${HOME}/.files"

GITHUB_URL="https://github.com/worr/dotfiles"

if [[ ! -x "${GIT}" ]]; then
	echo "Could not find git. Terminating."
	exit 1
fi

if [[ ! -x "${MAKE}" ]]; then
	MAKE=`which make`
fi

if [[ ! -x "${MAKE}" ]]; then
	echo "Could not find make. Terminating."
	exit 2
fi

if [[ ! -d ${INSTALL_DIR} ]]; then
	${GIT} clone "${GITHUB_URL}" "${INSTALL_DIR}"
fi

cd "${HOME}/.files"
${MAKE}

touch "${CRON_TEMP}"
${CRONTAB} -l > "${CRON_TEMP}" 2> /dev/null
CRON_ENTRY=$(${GREP} "${CRON_SCRIPT}" "${CRON_TEMP}")
if [[ "${CRON_ENTRY}" = "" ]]; then
	chmod 755 "${CRON_SCRIPT}"
	echo "0 1 * * * ${CRON_SCRIPT}" >> "${CRON_TEMP}"
	${CRONTAB} "${CRON_TEMP}"
fi

rm "${CRON_TEMP}"
