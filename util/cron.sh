#!/usr/bin/env bash

GIT=`which git`
MAKE=`which gmake`

if [[ ! -x ${MAKE} ]]; then
	MAKE=`which make`
fi

GITHUB_URL="https://github.com/worr/dotfiles"

cd "${HOME}/.files"
${GIT} pull origin master
${MAKE}
